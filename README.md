### Aula sobre o Git Flow
### Comandos Git Flow

```
git init → Iniciar o git flow, com as respectivas branchs, features, develop, main, bugfix.

git flow feature start [nome] → Esse comando vai gerar uma branch de features, aonde vai ser realizada uma nova funcionalidade.

git flow feature finish [nome] → Com esse comando vamos fazer um merge da feature criada para a develope. Finalizar essa feature, mas com essa linha de código você pode ferrar tudo porque se tiver mais gente desenvolvendo você vai finalizar essa branch, então o certo e usar o comando abaixo.

git flow feature publish [nome] → Vai pegar a branch e subir ela para o servidor externos gitHub ou gitLab

git flow feature pull [nome] → Se outros desenvolvedores estiverem desenvolvendo nessa msm branch, e ele realizar uma atualização no servidor, poderemos usar esse comando para a mantermos nosso código atualizado.

git flow release start 1.0  → Vamos imaginar que estamos desenvolvendo uma nova feature essa feature já foi realizada, já fizemos um merge dele em produção agora vamos criar uma release dessa branch de desenvolvimento, o que é essa release, uma branch de liberação uma nova versão sem bugs.

git flow release finish 1.0 → Esse comando vai fazer o merge de (Na branch release, está todo o merge de develop) release-branch para a branch main. Só que usando o finish ele vai finalizar a branch excluir. 

git flow release publish 1.0 → Esse comando vai fazer o merge de (Na branch release, está todo o merge de develop) release-branch para a branch main. Só que usando o publish ele não vai finalizar a branch excluir.

git flow hotfix start my_hotfix → Esse comando vai criar uma branch a partir da main, para a correção de bugs que estão na branch main.
```